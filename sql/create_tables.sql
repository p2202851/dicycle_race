CREATE TABLE carte
(
    id     INT AUTO_INCREMENT NOT NULL,
    img    VARCHAR(255),
    niveau VARCHAR(255),
    points INT,
    PRIMARY KEY (id)
);

CREATE TABLE choisitMain
(
    idP       INT NOT NULL,
    id_carte  INT NOT NULL,
    numT      INT NOT NULL,
    idJ       INT NOT NULL,
    nbDeRouge INT,
    nbDeJaune INT,
    nbDeBleu  INT,
    PRIMARY KEY (idP, idJ, numT, id_carte)
);

CREATE TABLE classement
(
    idC    INT AUTO_INCREMENT NOT NULL,
    nom    VARCHAR(255),
    portee VARCHAR(255),
    PRIMARY KEY (idC)
);

CREATE TABLE classementEquipe
(
    idC INT NOT NULL,
    PRIMARY KEY (idC)
);

CREATE TABLE classementIndividuel
(
    idC INT NOT NULL,
    PRIMARY KEY (idC)

);

CREATE TABLE contrain
(
    id_1 INT NOT NULL,
    id_2 INT NOT NULL,
    PRIMARY KEY (id_1, id_2)
);

CREATE TABLE contrainte
(
    id   INT AUTO_INCREMENT NOT NULL,
    nom  VARCHAR(255),
    coul VARCHAR(42),
    PRIMARY KEY (id)
);

CREATE TABLE deLance
(
    PRIMARY KEY (rang, couleur, valeur, numT, idL),
    rang    INT         NOT NULL,
    couleur VARCHAR(42) NOT NULL,
    valeur  INT         NOT NULL,
    id      INT         NOT NULL,
    numT    INT         NOT NULL,
    idL     INT         NOT NULL
);

CREATE TABLE effectue
(
    PRIMARY KEY (id),
    idJ  INT NOT NULL,
    id   INT NOT NULL,
    numT INT NOT NULL,
    idL  INT NOT NULL
);


CREATE TABLE equipe
(
    idE INT AUTO_INCREMENT NOT NULL,
    nom VARCHAR(255),
    PRIMARY KEY (idE)
);

CREATE TABLE estClasse
(
    idC  INT NOT NULL,
    idJ  INT NOT NULL,
    rang INT,
    PRIMARY KEY (idC, idJ)
);

CREATE TABLE estCompose
(
    id_plateau INT NOT NULL,
    id_carte   INT NOT NULL,
    ordre      INT NOT NULL,
    PRIMARY KEY (id_plateau, id_carte)
);

CREATE TABLE estEnLienAvec
(
    idT_1 INT NOT NULL,
    idT_2 INT NOT NULL,
    PRIMARY KEY (idT_1, idT_2)
);

CREATE TABLE estLieA
(
    idC_1    INT NOT NULL,
    idC_2    INT NOT NULL,
    typeLien VARCHAR(255),
    PRIMARY KEY (idC_1, idC_2)
);

CREATE TABLE faceDeDes
(
    id_1 INT NOT NULL,
    val  INT,
    PRIMARY KEY (id_1)
);

CREATE TABLE joue
(
    idJ   INT NOT NULL,
    id    INT NOT NULL,
    score INT,
    PRIMARY KEY (idJ, id)
);

CREATE TABLE joueur
(
    idJ      INT AUTO_INCREMENT NOT NULL,
    nom      VARCHAR(255),
    prenom   VARCHAR(255),
    pseudo   VARCHAR(255),
    anneNais DATE,
    email    VARCHAR(255),
    idE      INT,
    PRIMARY KEY (idJ)
);

CREATE TABLE lancer
(
    id   INT NOT NULL,
    numT INT NOT NULL,
    idL  INT NOT NULL,
    PRIMARY KEY (id, numT, idL)
);

CREATE TABLE lesMemesAuchoix
(
    id_1 INT NOT NULL,
    nb   INT,
    PRIMARY KEY (id_1)
);

CREATE TABLE participe
(
    idJ         INT          NOT NULL,
    idT         INT          NOT NULL,
    niveaux     VARCHAR(255) NOT NULL,
    aJoue       VARCHAR(255),
    estQualifie VARCHAR(255),
    PRIMARY KEY (idJ, idT, niveaux)
);

CREATE TABLE partie
(
    id            INT AUTO_INCREMENT NOT NULL,
    date          DATE,
    heure         TIME,
    duree         TIME,
    etats         VARCHAR(255),
    ordeDePassage VARCHAR(255),
    id_2          INT                NOT NULL,
    joueurEnCour  int                null,
    PRIMARY KEY (id)
);

CREATE TABLE phase
(
    idT     INT          NOT NULL,
    niveaux VARCHAR(255) NOT NULL,
    dateP   DATE,
    id      INT          NOT NULL,
    PRIMARY KEY (idT, niveaux)

);

CREATE TABLE pion
(
    id      INT NOT NULL,
    couleur VARCHAR(255),
    id_2    INT NOT NULL,
    id_3    INT NOT NULL,
    idJ     INT NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE plateau
(
    id     INT AUTO_INCREMENT NOT NULL,
    nCarte INT,
    PRIMARY KEY (id)
);

CREATE TABLE soeuiDeDes
(
    id_1  INT NOT NULL,
    seuil INT,
    sens  CHAR(1),
    PRIMARY KEY (id_1)
);

CREATE TABLE sontClasses
(
    idC  INT NOT NULL,
    idE  INT NOT NULL,
    rang INT,
    PRIMARY KEY (idC, idE)
);

CREATE TABLE suiteAuChoix
(
    id_1 INT NOT NULL,
    nb   INT,
    PRIMARY KEY (id_1)
);

CREATE TABLE tenteValidation
(
    id_1        INT NOT NULL,
    numT        INT NOT NULL,
    id_2        INT NOT NULL,
    idJ         INT NOT NULL,
    nbTentative INT,
    validee     VARCHAR(255),
    PRIMARY KEY (id_1, numT, id_2, idJ)
);

CREATE TABLE tour
(
    id   INT NOT NULL,
    numT INT NOT NULL,
    PRIMARY KEY (id, numT)
);

CREATE TABLE tournoi
(
    idT     INT AUTO_INCREMENT NOT NULL,
    nom     VARCHAR(255),
    dateDeb DATE,
    dateFin DATE,
    PRIMARY KEY (idT)
);



ALTER TABLE choisitMain
    ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);
ALTER TABLE choisitMain
    ADD FOREIGN KEY (idP, numT) REFERENCES tour (id, numT);

ALTER TABLE classementEquipe
    ADD FOREIGN KEY (idC) REFERENCES classement (idC);

ALTER TABLE classementIndividuel
    ADD FOREIGN KEY (idC) REFERENCES classement (idC);

ALTER TABLE contrain
    ADD FOREIGN KEY (id_2) REFERENCES carte (id);
ALTER TABLE contrain
    ADD FOREIGN KEY (id_1) REFERENCES contrainte (id);

ALTER TABLE effectue
    ADD FOREIGN KEY (id, numT, idL) REFERENCES lancer (id, numT, idL);
ALTER TABLE effectue
    ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);

ALTER TABLE estClasse
    ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);
ALTER TABLE estClasse
    ADD FOREIGN KEY (idC) REFERENCES classementIndividuel (idC);

ALTER TABLE estCompose
    ADD FOREIGN KEY (id_carte) REFERENCES carte (id);
ALTER TABLE estCompose
    ADD FOREIGN KEY (id_plateau) REFERENCES plateau (id);

ALTER TABLE estEnLienAvec
    ADD FOREIGN KEY (idT_2) REFERENCES tournoi (idT);
ALTER TABLE estEnLienAvec
    ADD FOREIGN KEY (idT_1) REFERENCES tournoi (idT);

ALTER TABLE estLieA
    ADD FOREIGN KEY (idC_2) REFERENCES classement (idC);
ALTER TABLE estLieA
    ADD FOREIGN KEY (idC_1) REFERENCES classement (idC);

ALTER TABLE faceDeDes
    ADD FOREIGN KEY (id_1) REFERENCES contrainte (id);

ALTER TABLE joue
    ADD FOREIGN KEY (id) REFERENCES partie (id);
ALTER TABLE joue
    ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);
ALTER TABLE joueur
    ADD FOREIGN KEY (idE) REFERENCES equipe (idE);

ALTER TABLE lancer
    ADD FOREIGN KEY (id, numT) REFERENCES tour (id, numT);

ALTER TABLE lesMemesAuchoix
    ADD FOREIGN KEY (id_1) REFERENCES contrainte (id);

ALTER TABLE participe
    ADD FOREIGN KEY (idT, niveaux) REFERENCES phase (idT, niveaux);
ALTER TABLE participe
    ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);

ALTER TABLE partie
    ADD FOREIGN KEY (id_2) REFERENCES plateau (id);
ALTER TABLE partie
    add constraint joueurEnCour
        foreign key (joueurEnCour) references joueur (idJ);


ALTER TABLE phase
    ADD FOREIGN KEY (id) REFERENCES partie (id);
ALTER TABLE phase
    ADD FOREIGN KEY (idT) REFERENCES tournoi (idT);

ALTER TABLE pion
    ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);
ALTER TABLE pion
    ADD FOREIGN KEY (id_3) REFERENCES carte (id);
ALTER TABLE pion
    ADD FOREIGN KEY (id_2) REFERENCES plateau (id);

ALTER TABLE soeuiDeDes
    ADD FOREIGN KEY (id_1) REFERENCES contrainte (id);

ALTER TABLE sontClasses
    ADD FOREIGN KEY (idE) REFERENCES equipe (idE);
ALTER TABLE sontClasses
    ADD FOREIGN KEY (idC) REFERENCES classementEquipe (idC);

ALTER TABLE suiteAuChoix
    ADD FOREIGN KEY (id_1) REFERENCES contrainte (id);

ALTER TABLE tenteValidation
    ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);
ALTER TABLE tenteValidation
    ADD FOREIGN KEY (id_2) REFERENCES carte (id);
ALTER TABLE tenteValidation
    ADD FOREIGN KEY (id_1, numT) REFERENCES tour (id, numT);

ALTER TABLE tour
    ADD FOREIGN KEY (id) REFERENCES partie (id);

ALTER TABLE deLance
    ADD FOREIGN KEY (id, numT, idL) REFERENCES lancer (id, numT, idL);
