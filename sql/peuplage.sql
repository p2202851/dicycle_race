INSERT INTO `classement` (`idC`, `nom`, `portee`) VALUES
(1, 'Classement ACP', 'internationale'),
(2, 'Classement Français des DicyclerRaces', 'nationale'),
(3, 'Classement ACEEP', 'internationale'),
(4, 'Classement Belge des DicyclerRaces', 'nationale'),
(5, 'Classement Auvergnats des DicyclerRaces', 'régionale');

INSERT INTO `equipe` (`idE`, `nom`) VALUES
(1, 'Les pédaleurs fous'),
(2, 'Les 42 du cyclos'),
(3, 'Les sans freins'),
(4, 'Les hazardicycles');

INSERT INTO `classementEquipe` (`idC`) VALUES
(3),


INSERT INTO `sontClasses` (`idC`,`idE`,`rang`) VALUES
(3, 1, 3),
(3, 2, 2),
(3, 3, 1);

INSERT INTO `joueur` (`idJ`, `nom`, `prenom`, `pseudo`, `anneNais`, `email`, `idE`) VALUES
(1, 'ATOUTALURE', 'Alphonse', 'AA99', '1999-06-12', NULL, 1),
(2, 'AVITE', 'Yves', 'YA01', '2001-12-06', NULL, 1),
(3, 'HERE', 'Axel', 'AH02', '2002-02-04', NULL, 1),
(4, 'LENTI', 'Sarah', 'SL98', '1998-05-11', NULL, 2),
(5, 'BAUL', 'Pat', 'PB01', '2001-10-06', NULL, 2),
(6, 'DESFREINS', 'Ella', 'ED00', '2000-05-05', NULL, 2),
(7, 'RALENTI', 'Sam', 'SR99', '1999-07-07', NULL, NULL),
(8, 'PARTIR', 'Eva', 'EP97', '1997-09-21', NULL, NULL),
(9, 'HERE', 'Axel', 'AH00', '2000-09-09', NULL, NULL),
(10, 'NAUBERDEAU', 'René-Jean', 'RJN', '1956-07-08', NULL, NULL);

INSERT INTO `classementIndividuel` (`idC`) VALUES
(1),
(2),
(4),
(5);

INSERT INTO `estClasse` (`idC`, `idJ`, `rang`) VALUES
(1, 4, 3),
(1, 6, 2),
(1, 7, 1),
(2, 5, 3),
(2, 8, 2),
(2, 10, 1),
(4, 5, 3),
(4, 6, 2),
(4, 7, 1),
(5, 3, 2),
(5, 9, 1),
(5, 10, 3);

INSERT INTO carte (
  id,
  img,
  niveau,
  points
)
SELECT
    DISTINCT id_carte,
    fichier,
    niveau,
    points
FROM
    donnees_fournies.instances2;

INSERT INTO contrainte (
  id,
  nom,
  coul
)
SELECT
    DISTINCT id_contrainte,
    nom,
    couleur
FROM  
  donnees_fournies.instances2;

INSERT INTO faceDeDes (
  id_1,
  val
  )
SELECT
    DISTINCT id_contrainte,
    valeur
FROM 
  donnees_fournies.instances2
WHERE
  nom = "face_de_dé";

INSERT INTO soeuiDeDes (
  id_1,
  seuil,
  sens
)
SELECT
    DISTINCT id_contrainte,
    valeur,
    sens
FROM 
  donnees_fournies.instances2
WHERE 
  nom = "seuil_de_dé";

INSERT INTO suiteAuChoix (
  id_1,
  nb
)
SELECT
    DISTINCT id_contrainte,
    valeur
FROM 
  donnees_fournies.instances2
WHERE 
  nom = "suite_au_choix";

INSERT INTO lesMemesAuchoix ( id_1, nb ) 
SELECT DISTINCT id_contrainte, valeur 
FROM donnees_fournies.instances2 
WHERE nom = "meme_au_choix" 


--todo

INSERT INTO tour(id, numT)
SELECT DISTINCT id_partie, numTour
FROM donnees_fournies.instances3;

INSERT INTO lancer (id, numT, idL)
SELECT DISTINCT id_partie, numTour, numL
FROM donnees_fournies.instances3;

INSERT INTO choisitMain(idP, id_carte, numT, idJ, nbDeBleu, nbDeRouge, nbDeJaune)
SELECT DISTINCT id_partie, carteAvalider, numTour, id_joueur, main_nb_bleu, main_nb_rouge, main_nb_jaune
FROM donnees_fournies.instances3;

---- todoo

INSERT INTO tenteValidation(idJ,id_2, id_1, numT)
SELECT DISTINCT id_joueur, carteAvalider, id_partie, numTour
FROM donnees_fournies.instances3;


INSERT INTO effectue (id, idJ, rang, couleur, valeur, id_carte, numT, idL)
SELECT DISTINCT id_partie, id_joueur, ???, SUBSTR(de1, 1, 1), SUBSTR(de1, 3, 3), carteAvalider, numTour, numL
FROM donnees_fournies.instances3

INSERT INTO contrain(id_1, id_2) 
SELECT id_contrainte, id_carte 
FROM instances2 