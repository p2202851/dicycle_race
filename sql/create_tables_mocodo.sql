CREATE TABLE carte (
  PRIMARY KEY (id),
  id     VARCHAR(42) NOT NULL,
  img    VARCHAR(42),
  niveau VARCHAR(42),
  points VARCHAR(42)
);

CREATE TABLE choisitMain (
  PRIMARY KEY (id, numT, idJ),
  id        VARCHAR(42) NOT NULL,
  numT      VARCHAR(42) NOT NULL,
  idJ       VARCHAR(42) NOT NULL,
  nbDeRouge VARCHAR(42),
  nbDeJaune VARCHAR(42),
  nbDeBleu  VARCHAR(42)
);

CREATE TABLE classement (
  PRIMARY KEY (idC),
  idC     VARCHAR(42) NOT NULL,
  nom     VARCHAR(42),
  porteee VARCHAR(42)
);

CREATE TABLE classementEquipe (
  PRIMARY KEY (idC),
  idC VARCHAR(42) NOT NULL
);

CREATE TABLE classementIndividuel (
  PRIMARY KEY (idC),
  idC VARCHAR(42) NOT NULL
);

CREATE TABLE contrain (
  PRIMARY KEY (id_1, id_2),
  id_1 VARCHAR(42) NOT NULL,
  id_2 VARCHAR(42) NOT NULL
);

CREATE TABLE contrainte (
  PRIMARY KEY (id),
  id   VARCHAR(42) NOT NULL,
  nom  VARCHAR(42),
  coul VARCHAR(42)
);

CREATE TABLE deLance (
  PRIMARY KEY (rang, couleur, valeur),
  rang    VARCHAR(42) NOT NULL,
  couleur VARCHAR(42) NOT NULL,
  valeur  VARCHAR(42) NOT NULL,
  id      VARCHAR(42) NOT NULL,
  numT    VARCHAR(42) NOT NULL,
  idL     VARCHAR(42) NOT NULL
);

CREATE TABLE effectue (
  PRIMARY KEY (idJ, id, numT, idL),
  idJ  VARCHAR(42) NOT NULL,
  id   VARCHAR(42) NOT NULL,
  numT VARCHAR(42) NOT NULL,
  idL  VARCHAR(42) NOT NULL
);

CREATE TABLE equipe (
  PRIMARY KEY (idE),
  idE VARCHAR(42) NOT NULL,
  nom VARCHAR(42)
);

CREATE TABLE estClasse (
  PRIMARY KEY (idC, idJ),
  idC  VARCHAR(42) NOT NULL,
  idJ  VARCHAR(42) NOT NULL,
  rang VARCHAR(42)
);

CREATE TABLE estCompose (
  PRIMARY KEY (id_1, id_2),
  id_1 VARCHAR(42) NOT NULL,
  id_2 VARCHAR(42) NOT NULL
);

CREATE TABLE estEnLienAvec (
  PRIMARY KEY (idT_1, idT_2),
  idT_1 VARCHAR(42) NOT NULL,
  idT_2 VARCHAR(42) NOT NULL
);

CREATE TABLE estLieA (
  PRIMARY KEY (idC_1, idC_2),
  idC_1    VARCHAR(42) NOT NULL,
  idC_2    VARCHAR(42) NOT NULL,
  typeLien VARCHAR(42)
);

CREATE TABLE faceDeDes (
  PRIMARY KEY (id),
  id  VARCHAR(42) NOT NULL,
  val VARCHAR(42)
);

CREATE TABLE joue (
  PRIMARY KEY (idJ, id),
  idJ   VARCHAR(42) NOT NULL,
  id    VARCHAR(42) NOT NULL,
  score VARCHAR(42)
);

CREATE TABLE joueur (
  PRIMARY KEY (idJ),
  idJ      VARCHAR(42) NOT NULL,
  nom      VARCHAR(42),
  prenom   VARCHAR(42),
  pseudo   VARCHAR(42),
  anneNais VARCHAR(42),
  email    VARCHAR(42),
  idE      VARCHAR(42) NULL
);

CREATE TABLE lancer (
  PRIMARY KEY (id, numT, idL),
  id   VARCHAR(42) NOT NULL,
  numT VARCHAR(42) NOT NULL,
  idL  VARCHAR(42) NOT NULL
);

CREATE TABLE lesMemesAuchoix (
  PRIMARY KEY (id),
  id VARCHAR(42) NOT NULL,
  nb VARCHAR(42)
);

CREATE TABLE participe (
  PRIMARY KEY (idJ, idT, niveaux),
  idJ         VARCHAR(42) NOT NULL,
  idT         VARCHAR(42) NOT NULL,
  niveaux     VARCHAR(42) NOT NULL,
  aJoue       VARCHAR(42),
  estQualifie VARCHAR(42)
);

CREATE TABLE partie (
  PRIMARY KEY (id),
  id            VARCHAR(42) NOT NULL,
  date          VARCHAR(42),
  heure         VARCHAR(42),
  duree         VARCHAR(42),
  etats         VARCHAR(42),
  ordeDePassage VARCHAR(42),
  id_2          VARCHAR(42) NOT NULL,
  UNIQUE (id_2)
);

CREATE TABLE phase (
  PRIMARY KEY (idT, niveaux),
  idT     VARCHAR(42) NOT NULL,
  niveaux VARCHAR(42) NOT NULL,
  dateP   VARCHAR(42),
  id      VARCHAR(42) NOT NULL,
  UNIQUE (id)
);

CREATE TABLE pion (
  PRIMARY KEY (id),
  id      VARCHAR(42) NOT NULL,
  couleur VARCHAR(42),
  id_2    VARCHAR(42) NOT NULL,
  id_3    VARCHAR(42) NOT NULL,
  idJ     VARCHAR(42) NOT NULL
);

CREATE TABLE plateau (
  PRIMARY KEY (id),
  id     VARCHAR(42) NOT NULL,
  nCarte VARCHAR(42)
);

CREATE TABLE soeuiDeDes (
  PRIMARY KEY (id),
  id    VARCHAR(42) NOT NULL,
  seuil VARCHAR(42),
  sens  VARCHAR(42)
);

CREATE TABLE sontClasses (
  PRIMARY KEY (idC, idE),
  idC  VARCHAR(42) NOT NULL,
  idE  VARCHAR(42) NOT NULL,
  rang VARCHAR(42)
);

CREATE TABLE suiteAuChoix (
  PRIMARY KEY (id),
  id VARCHAR(42) NOT NULL,
  nb VARCHAR(42)
);

CREATE TABLE tenteValidation (
  PRIMARY KEY (id_1, numT, id_2, idJ),
  id_1        VARCHAR(42) NOT NULL,
  numT        VARCHAR(42) NOT NULL,
  id_2        VARCHAR(42) NOT NULL,
  idJ         VARCHAR(42) NOT NULL,
  nbTentative VARCHAR(42),
  validee     VARCHAR(42)
);

CREATE TABLE tour (
  PRIMARY KEY (id, numT),
  id   VARCHAR(42) NOT NULL,
  numT VARCHAR(42) NOT NULL
);

CREATE TABLE tournoi (
  PRIMARY KEY (idT),
  idT     VARCHAR(42) NOT NULL,
  nom     VARCHAR(42),
  dateDeb VARCHAR(42),
  dateFin VARCHAR(42)
);

ALTER TABLE choisitMain ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);
ALTER TABLE choisitMain ADD FOREIGN KEY (id, numT) REFERENCES tour (id, numT);

ALTER TABLE classementEquipe ADD FOREIGN KEY (idC) REFERENCES classement (idC);

ALTER TABLE classementIndividuel ADD FOREIGN KEY (idC) REFERENCES classement (idC);

ALTER TABLE contrain ADD FOREIGN KEY (id_2) REFERENCES carte (id);
ALTER TABLE contrain ADD FOREIGN KEY (id_1) REFERENCES contrainte (id);

ALTER TABLE deLance ADD FOREIGN KEY (id, numT, idL) REFERENCES lancer (id, numT, idL);

ALTER TABLE effectue ADD FOREIGN KEY (id, numT, idL) REFERENCES lancer (id, numT, idL);
ALTER TABLE effectue ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);

ALTER TABLE estClasse ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);
ALTER TABLE estClasse ADD FOREIGN KEY (idC) REFERENCES classementIndividuel (idC);

ALTER TABLE estCompose ADD FOREIGN KEY (id_2) REFERENCES carte (id);
ALTER TABLE estCompose ADD FOREIGN KEY (id_1) REFERENCES plateau (id);

ALTER TABLE estEnLienAvec ADD FOREIGN KEY (idT_2) REFERENCES tournoi (idT);
ALTER TABLE estEnLienAvec ADD FOREIGN KEY (idT_1) REFERENCES tournoi (idT);

ALTER TABLE estLieA ADD FOREIGN KEY (idC_2) REFERENCES classement (idC);
ALTER TABLE estLieA ADD FOREIGN KEY (idC_1) REFERENCES classement (idC);

ALTER TABLE faceDeDes ADD FOREIGN KEY (id) REFERENCES contrainte (id);

ALTER TABLE joue ADD FOREIGN KEY (id) REFERENCES partie (id);
ALTER TABLE joue ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);
ALTER TABLE joueur ADD FOREIGN KEY (idE) REFERENCES equipe (idE);

ALTER TABLE lancer ADD FOREIGN KEY (id, numT) REFERENCES tour (id, numT);

ALTER TABLE lesMemesAuchoix ADD FOREIGN KEY (id) REFERENCES contrainte (id);

ALTER TABLE participe ADD FOREIGN KEY (idT, niveaux) REFERENCES phase (idT, niveaux);
ALTER TABLE participe ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);

ALTER TABLE partie ADD FOREIGN KEY (id_2) REFERENCES plateau (id);

ALTER TABLE phase ADD FOREIGN KEY (id) REFERENCES partie (id);
ALTER TABLE phase ADD FOREIGN KEY (idT) REFERENCES tournoi (idT);

ALTER TABLE pion ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);
ALTER TABLE pion ADD FOREIGN KEY (id_3) REFERENCES carte (id);
ALTER TABLE pion ADD FOREIGN KEY (id_2) REFERENCES plateau (id);

ALTER TABLE soeuiDeDes ADD FOREIGN KEY (id) REFERENCES contrainte (id);

ALTER TABLE sontClasses ADD FOREIGN KEY (idE) REFERENCES equipe (idE);
ALTER TABLE sontClasses ADD FOREIGN KEY (idC) REFERENCES classementEquipe (idC);

ALTER TABLE suiteAuChoix ADD FOREIGN KEY (id) REFERENCES contrainte (id);

ALTER TABLE tenteValidation ADD FOREIGN KEY (idJ) REFERENCES joueur (idJ);
ALTER TABLE tenteValidation ADD FOREIGN KEY (id_2) REFERENCES carte (id);
ALTER TABLE tenteValidation ADD FOREIGN KEY (id_1, numT) REFERENCES tour (id, numT);

ALTER TABLE tour ADD FOREIGN KEY (id) REFERENCES partie (id);

