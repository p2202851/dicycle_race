CREATE TABLE carte (
  id      INT AUTO_INCREMENT NOT NULL,
  img     VARCHAR(255),
  niveau  VARCHAR(255),
  points  INT,
  PRIMARY KEY (id)
);

CREATE TABLE choisitMain (
  id         INT AUTO_INCREMENT NOT NULL,
  id_carte   INT NOT NULL,
  numT       INT NOT NULL,
  idJ        INT NOT NULL,
  nbDeRouge  INT,
  nbDeJaune  INT,
  nbDeBleu   INT,
  PRIMARY KEY (id),
  FOREIGN KEY (idJ) REFERENCES joueur (idJ),
  FOREIGN KEY (id_carte) REFERENCES carte (id)
);

CREATE TABLE classement (
  idC     INT AUTO_INCREMENT NOT NULL,
  nom     VARCHAR(255),
  portee  VARCHAR(255),
  PRIMARY KEY (idC)
);

CREATE TABLE classementEquipe (
  idC INT NOT NULL,
  PRIMARY KEY (idC),
  FOREIGN KEY (idC) REFERENCES classement (idC)
);

CREATE TABLE classementIndividuel (
  idC INT NOT NULL,
  PRIMARY KEY (idC),
  FOREIGN KEY (idC) REFERENCES classement (idC)
);

CREATE TABLE contrain (
  id_1 INT NOT NULL,
  id_2 INT NOT NULL,
  PRIMARY KEY (id_1, id_2),
  FOREIGN KEY (id_1) REFERENCES contrainte (id),
  FOREIGN KEY (id_2) REFERENCES carte (id)
);

CREATE TABLE contrainte (
  id  INT AUTO_INCREMENT NOT NULL,
  nom VARCHAR(255),
  coul VARCHAR(42),
  PRIMARY KEY (id)
);

CREATE TABLE effectue (
  id       INT AUTO_INCREMENT NOT NULL,
  idJ      INT NOT NULL,
  rang     INT NOT NULL,
  couleur  VARCHAR(255) NOT NULL,
  valeur   INT NOT NULL,
  id_carte INT NOT NULL,
  numT     INT NOT NULL,
  idL      INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (idJ) REFERENCES joueur (idJ),
  FOREIGN KEY (id_carte) REFERENCES carte (id),
  FOREIGN KEY (id, numT, idL) REFERENCES lancer (id, numT, idL)
);

CREATE TABLE equipe (
  idE INT AUTO_INCREMENT NOT NULL,
  nom VARCHAR(255),
  PRIMARY KEY (idE)
);

CREATE TABLE estClasse (
  idC  INT NOT NULL,
  idJ  INT NOT NULL,
  rang INT,
  PRIMARY KEY (idC, idJ),
  FOREIGN KEY (idJ) REFERENCES joueur (idJ),
  FOREIGN KEY (idC) REFERENCES classementIndividuel (idC)
);

CREATE TABLE estCompose (
  id_plateau INT NOT NULL,
  id_carte   INT NOT NULL,
  PRIMARY KEY (id_plateau, id_carte),
  FOREIGN KEY (id_plateau) REFERENCES plateau (id),
  FOREIGN KEY (id_carte) REFERENCES carte (id)
);

CREATE TABLE estEnLienAvec (
  idT_1 INT NOT NULL,
  idT_2 INT NOT NULL,
  PRIMARY KEY (idT_1, idT_2),
  FOREIGN KEY (idT_1) REFERENCES tournoi (idT),
  FOREIGN KEY (idT_2) REFERENCES tournoi (idT)
);

CREATE TABLE estLieA (
  idC_1    INT NOT NULL,
  idC_2    INT NOT NULL,
  typeLien VARCHAR(255),
  PRIMARY KEY (idC_1, idC_2),
  FOREIGN KEY (idC_1) REFERENCES classement (idC),
  FOREIGN KEY (idC_2) REFERENCES classement (idC)
);

CREATE TABLE faceDeDes (
  id_1 INT NOT NULL,
  id_2 INT,
  val  INT,
  PRIMARY KEY (id_1),
  FOREIGN KEY (id_1) REFERENCES contrainte (id)
);

CREATE TABLE joue (
  idJ   INT NOT NULL,
  id    INT NOT NULL,
  score INT,
  PRIMARY KEY (idJ, id),
  FOREIGN KEY (id) REFERENCES partie (id),
  FOREIGN KEY (idJ) REFERENCES joueur (idJ)
);

CREATE TABLE joueur (
  idJ      INT AUTO_INCREMENT NOT NULL,
  nom      VARCHAR(255),
  prenom   VARCHAR(255),
  pseudo   VARCHAR(255),
  anneNais DATE,
  email    VARCHAR(255),
  idE      INT,
  PRIMARY KEY (idJ),
  FOREIGN KEY (idE) REFERENCES equipe (idE)
);

CREATE TABLE lancer (
  id    INT NOT NULL,
  numT  INT NOT NULL,
  idL   INT NOT NULL,
  PRIMARY KEY (id, numT, idL),
  FOREIGN KEY (id, numT) REFERENCES tour (id, numT)
);

CREATE TABLE lesMemesAuchoix (
  id_1 INT NOT NULL,
  id_2 INT,
  nb   INT,
  PRIMARY KEY (id_1),
  FOREIGN KEY (id_1) REFERENCES contrainte (id)
);

CREATE TABLE participe (
  idJ         INT NOT NULL,
  idT         INT NOT NULL,
  niveaux     VARCHAR(255) NOT NULL,
  aJoue       VARCHAR(255),
  estQualifie VARCHAR(255),
  PRIMARY KEY (idJ, idT, niveaux),
  FOREIGN KEY (idT, niveaux) REFERENCES phase (idT, niveaux),
  FOREIGN KEY (idJ) REFERENCES joueur (idJ)
);

CREATE TABLE partie (
  id            INT AUTO_INCREMENT NOT NULL,
  date          DATE,
  heure         TIME,
  duree         TIME,
  etats         VARCHAR(255),
  ordeDePassage VARCHAR(255),
  id_2          INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_2) REFERENCES plateau (id)
);

CREATE TABLE phase (
  idT     INT NOT NULL,
  niveaux VARCHAR(255) NOT NULL,
  dateP   DATE,
  id      INT NOT NULL,
  PRIMARY KEY (idT, niveaux),
  FOREIGN KEY (id) REFERENCES partie (id),
  FOREIGN KEY (idT) REFERENCES tournoi (idT)
);

CREATE TABLE pion (
  id      INT NOT NULL,
  couleur VARCHAR(255),
  id_2    INT NOT NULL,
  id_3    INT NOT NULL,
  idJ     INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (idJ) REFERENCES joueur (idJ),
  FOREIGN KEY (id_3) REFERENCES carte (id),
  FOREIGN KEY (id_2) REFERENCES plateau (id)
);

CREATE TABLE plateau (
  id     INT AUTO_INCREMENT NOT NULL,
  nCarte INT,
  PRIMARY KEY (id)
);

CREATE TABLE soeuiDeDes (
  id_1  INT NOT NULL,
  id_2  INT,
  seuil INT,
  sens  VARCHAR(255),
  PRIMARY KEY (id_1),
  FOREIGN KEY (id_1) REFERENCES contrainte (id)
);

CREATE TABLE sontClasses (
  idC  INT NOT NULL,
  idE  INT NOT NULL,
  rang INT,
  PRIMARY KEY (idC, idE),
  FOREIGN KEY (idE) REFERENCES equipe (idE),
  FOREIGN KEY (idC) REFERENCES classementEquipe (idC)
);

CREATE TABLE suiteAuChoix (
  id_1 INT NOT NULL,
  id_2 INT,
  nb   INT,
  PRIMARY KEY (id_1),
  FOREIGN KEY (id_1) REFERENCES contrainte (id)
);

CREATE TABLE tenteValidation (
  id_1        INT NOT NULL,
  numT        INT NOT NULL,
  id_2        INT NOT NULL,
  idJ         INT NOT NULL,
  nbTentative INT,
  validee     VARCHAR(255),
  PRIMARY KEY (id_1, numT, id_2, idJ),
  FOREIGN KEY (idJ) REFERENCES joueur (idJ),
  FOREIGN KEY (id_2) REFERENCES carte (id),
  FOREIGN KEY (id_1, numT) REFERENCES tour (id, numT)
);

CREATE TABLE tour (
  id   INT NOT NULL,
  numT INT NOT NULL,
  PRIMARY KEY (id, numT),
  FOREIGN KEY (id) REFERENCES partie (id)
);

CREATE TABLE tournoi (
  idT     INT AUTO_INCREMENT NOT NULL,
  nom     VARCHAR(255),
  dateDeb DATE,
  dateFin DATE,
  PRIMARY KEY (idT)
);
