-- --------------------------------------------------------

--
-- Structure de la table `instances1`
--

CREATE TABLE `instances1` (
  `id_partie` int(11) NOT NULL,
  `dateP` date DEFAULT NULL,
  `HeureP` varchar(5) DEFAULT NULL,
  `durée_secondes` int(11) DEFAULT NULL,
  `c1` int(11) DEFAULT NULL,
  `c2` int(11) DEFAULT NULL,
  `c3` int(11) DEFAULT NULL,
  `c4` int(11) DEFAULT NULL,
  `c5` int(11) DEFAULT NULL,
  `c6` int(11) DEFAULT NULL,
  `c7` int(11) DEFAULT NULL,
  `c8` int(11) DEFAULT NULL,
  `c9` int(11) DEFAULT NULL,
  `c10` int(11) DEFAULT NULL,
  `c11` int(11) DEFAULT NULL,
  `c12` int(11) DEFAULT NULL,
  `id_joueur` int(11) NOT NULL,
  `nom` varchar(80) DEFAULT NULL,
  `prénom` varchar(80) DEFAULT NULL,
  `pseudo` varchar(80) DEFAULT NULL,
  `date_naiss` date DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `équipe` varchar(50) DEFAULT NULL,
  `cumul_points_cartes` decimal(32,0) DEFAULT NULL,
  `rang_arrivee` bigint(21) NOT NULL DEFAULT 0,
  `nb_joueurs_partie` bigint(21) NOT NULL DEFAULT 0,
  `score_final_partie` decimal(54,0) DEFAULT NULL,
  `état` varchar(50) DEFAULT 'T',
  `strategie` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;



CREATE TABLE `instances3` (
  `id_partie` int(11) NOT NULL,
  `id_joueur` int(11) NOT NULL,
  `numTour` int(11) NOT NULL,
  `numL` int(11) NOT NULL,
  `carteAvalider` int(11) DEFAULT NULL,
  `de1` varchar(3) DEFAULT NULL,
  `de2` varchar(3) DEFAULT NULL,
  `de3` varchar(3) DEFAULT NULL,
  `de4` varchar(3) DEFAULT NULL,
  `de5` varchar(3) DEFAULT NULL,
  `de6` varchar(3) DEFAULT NULL,
  `main_nb_bleu` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_nb_jaune` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `main_nb_rouge` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `instancesTest`
--

CREATE TABLE `instancesTest` (
  `A` int(11) DEFAULT NULL,
  `B` int(11) DEFAULT NULL,
  `C` varchar(50) DEFAULT NULL,
  `D` int(11) DEFAULT NULL,
  `E` int(11) DEFAULT NULL,
  `F` int(11) DEFAULT NULL,
  `G` int(11) DEFAULT NULL,
  `H` varchar(50) DEFAULT NULL,
  `I` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `instancesTest`
--

INSERT INTO `instancesTest` (`A`, `B`, `C`, `D`, `E`, `F`, `G`, `H`, `I`) VALUES
(1, 5, 'v1', 101, 201, 301, 10, 'X*1', 'a'),
(1, 5, 'v1', 101, 201, 301, 20, 'Y*2', 'b'),
(2, 10, 'v2', 102, 202, 302, 30, 'Z*1', 'a'),
(2, 10, 'v2', 102, 202, 302, 40, 'X*1', 'b'),
(3, 1, 'v3', 101, 201, 301, 50, 'Z*3', 'a');

-- --------------------------------------------------------

--
-- Structure de la table `joueur`
--
----------------------------------------------

--
-- Structure de la table `participe`
--

CREATE TABLE `participe` (
  `idT` int(11) NOT NULL,
  `niveau` varchar(50) NOT NULL,
  `idJ` int(11) NOT NULL,
  `a_joué` tinyint(1) DEFAULT NULL,
  `est_qualifié` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `participe`
--

INSERT INTO `participe` (`idT`, `niveau`, `idJ`, `a_joué`, `est_qualifié`) VALUES
(1, 'Demi-finale', 1, 1, 1),
(1, 'Demi-finale', 4, 1, 1),
(1, 'Finale', 1, 1, 0),
(1, 'Finale', 4, 1, 1),
(1, 'Quart de finale', 1, 1, 1),
(1, 'Quart de finale', 2, 1, 0),
(1, 'Quart de finale', 3, 1, 0),
(1, 'Quart de finale', 4, 1, 1),
(2, 'Finale', 5, 1, 0),
(2, 'Finale', 6, 1, 1),
(3, 'Finale', 7, 1, 0),
(3, 'Finale', 8, 1, 1),
(4, 'Demi-finale', 2, 1, 1),
(4, 'Demi-finale', 10, 1, 1),
(4, 'Finale', 2, 1, 0),
(4, 'Finale', 10, 1, 1),
(4, 'Quart de finale', 2, 1, 1),
(4, 'Quart de finale', 5, 1, 0),
(4, 'Quart de finale', 9, 1, 0),
(4, 'Quart de finale', 10, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `phase`
--

CREATE TABLE `phase` (
  `idT` int(11) NOT NULL,
  `niveau` varchar(50) NOT NULL,
  `dateP` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `phase`
--

INSERT INTO `phase` (`idT`, `niveau`, `dateP`) VALUES
(1, 'Demi-finale', '2024-01-25'),
(1, 'Finale', '2024-01-30'),
(1, 'Huitième de finale', '2024-01-21'),
(1, 'Quart', '2024-01-20'),
(1, 'Quart de finale', '2024-01-22'),
(1, 'Sélection', '2024-01-15'),
(2, 'Demi-finale', '2024-02-25'),
(2, 'Finale', '2024-02-29'),
(2, 'Huitième de finale', '2024-02-21'),
(2, 'Quart', '2024-02-20'),
(2, 'Quart de finale', '2024-02-22'),
(2, 'Sélection', '2024-02-15'),
(3, 'Demi-finale', '2024-03-25'),
(3, 'Finale', '2024-03-30'),
(3, 'Huitième de finale', '2024-03-21'),
(3, 'Quart', '2024-03-20'),
(3, 'Quart de finale', '2024-03-22'),
(3, 'Sélection', '2024-03-15'),
(4, 'Demi-finale', '2023-04-25'),
(4, 'Finale', '2023-04-30'),
(4, 'Huitième de finale', '2023-04-21'),
(4, 'Quart', '2023-04-20'),
(4, 'Quart de finale', '2023-04-22'),
(4, 'Sélection', '2023-04-15'),
(5, 'Demi-finale', '2022-04-25'),
(5, 'Finale', '2022-04-30'),
(5, 'Huitième de finale', '2022-04-21'),
(5, 'Quart', '2022-04-20'),
(5, 'Quart de finale', '2022-04-22'),
(5, 'Sélection', '2022-04-15');

-- --------------------------------------------------------

--
-- Structure de la table `tournoi`
--

CREATE TABLE `tournoi` (
  `idT` int(11) NOT NULL,
  `nom` varchar(80) DEFAULT NULL,
  `dateDeb` date DEFAULT NULL,
  `dateFin` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Déchargement des données de la table `tournoi`
--

INSERT INTO `tournoi` (`idT`, `nom`, `dateDeb`, `dateFin`) VALUES
(1, 'DicyRace', '2024-01-15', '2024-01-30'),
(2, 'DicyMasterRace', '2024-02-15', '2024-02-29'),
(3, 'Masters of DicyRace', '2024-03-15', '2024-03-30'),
(4, 'DyRacer Masters', '2023-04-15', '2023-04-30'),
(5, 'DyRacerCompet', '2023-04-15', '2023-04-30');

-- --------------------------------------------------------
