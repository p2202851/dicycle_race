<?php
global $connexion;

// Vérifier si l'ID de la partie est passé dans l'URL
if (isset($_GET['id_partie'])) {
    $id_partie = $_GET['id_partie'];

    // Vérifier si le formulaire a été soumis pour ajouter un joueur à la partie
    if (isset($_POST['ajouter_joueur'])) {
        // Récupérer l'ID du joueur sélectionné
        $id_joueur = $_POST['joueur'];

        // Insérer le joueur dans la table de liaison entre la partie et les joueurs
        $query = $connexion->prepare("INSERT INTO joue (idJ, id, score) VALUES (?, ?, 0)");
        $query->bind_param("ii", $id_joueur, $id_partie);
        $query->execute();

        echo "<div class='success'>Joueur ajouté avec succès à la partie.</div>";
        $query = $connexion->prepare("UPDATE partie SET nbJoueurPartie = nbJoueurPartie + 1 WHERE id=?");
        $query->bind_param("i", $id_partie);
        $query->execute();
        $couleurs = ["rouge", "bleu", "vert", "jaune"];
        $couleur = $couleurs[rand(0, 3)];

        $query = $connexion->prepare("SELECT id_2 as id_plateau FROM partie WHERE id=?");
        $query->bind_param("i", $id_partie);
        $query->execute();
        $id_plateau = $query->get_result()->fetch_assoc()['id_plateau'];

        $sql_add_pion = $connexion->prepare("INSERT INTO pion (couleur, id_2, idJ) VALUES (?, ?, ?)");
        $sql_add_pion->bind_param("sii", $couleur, $id_plateau, $id_joueur);
        $sql_add_pion->execute();

    }

    // Vérifier si le formulaire a été soumis pour retirer un joueur de la partie
    if (isset($_POST['retirer_joueur'])) {
        // Récupérer l'ID du joueur à retirer
        $id_joueur_retirer = $_POST['joueur_retirer'];

        // Supprimer le joueur de la table de liaison entre la partie et les joueurs
        $query = $connexion->prepare("DELETE FROM joue WHERE idJ=? AND id=?");
        $query->bind_param("ii", $id_joueur_retirer, $id_partie);
        $query->execute();
        echo "<div class='success'>Joueur retiré avec succès de la partie.</div>";
        $query = $connexion->prepare("UPDATE partie SET nbJoueurPartie = nbJoueurPartie - 1 WHERE id=?");
        $query->bind_param("i", $id_partie);
        $query->execute();
        $query = $connexion->prepare("DELETE FROM pion WHERE idJ=? AND id_2=(SELECT id_2 FROM partie WHERE id=?)");
        $query->bind_param("ii", $id_joueur_retirer, $id_partie);
        $query->execute();

    }

    // Sélectionner tous les joueurs en base de données
    $sql = "SELECT idJ, nom, prenom, pseudo FROM joueur";
    $joueurs = $connexion->query($sql);

    // Sélectionner les joueurs déjà ajoutés à la partie avec leurs détails
    $query = $connexion->prepare("SELECT j.idJ, j.nom, j.prenom, j.pseudo FROM joueur j INNER JOIN joue pj ON j.idJ = pj.idJ WHERE pj.id=?");
    $query->bind_param("i", $id_partie);
    $query->execute();
    $result_partie_joueurs = $query->get_result();

    if (!$result_partie_joueurs) {
        echo "<div class='error'>Une erreur s'est produite lors de la récupération des joueurs de la partie : " . $connexion->error . "</div>";
    }

} else {
    echo "<div class='error'>ID de la partie non spécifié.</div>";
}
?>
