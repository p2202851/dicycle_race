<?php

//$id = 1;
$id = fetchPartieId();

if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'start':
            $date = date('Y-m-d', time());
            $hour = date('H:i:s', time());
            saveBegin($id, $date, $hour);
            $firstPlayer = fetchJoueurs($id)[0];
            setCurrentPlayer($id, $firstPlayer['idJ']);
            everyOneToFirstCase($id);
            createNewTour($id);
            $state = "chooseDice";
            break;
        case 'chooseDice':
            $blueCount = $_POST['blue'];
            $yellowCount = $_POST['yellow'];
            $redCount = $_POST['red'];
            saveMain($id, $blueCount, $yellowCount, $redCount);
            $state = "chosenDice";
            break;
        case 'roll':
            $numTour = getTourID($id);
            if (3 - getLanceCount($id, $numTour, getCurrentPlayer($id)['idJ']) > 0) {
                $main = getMain($id, getCurrentPlayer($id)['idJ']);
                $blueCount = $main['nbDeBleu'];
                $yellowCount = $main['nbDeJaune'];
                $redCount = $main['nbDeRouge'];
                //generate the dice values
                $blueValues = [];
                $yellowValues = [];
                $redValues = [];
                for ($i = 0; $i < $blueCount; $i++) $blueValues[] = rand(1, 6);
                for ($i = 0; $i < $yellowCount; $i++) $yellowValues[] = rand(1, 6);
                for ($i = 0; $i < $redCount; $i++) $redValues[] = rand(1, 6);
                //save the dice values
                saveDiceValues($id, $blueValues, $yellowValues, $redValues);
            }
            break;
        case 'endTour':
            $players = fetchJoueurs($id);
            $currentPlayer = getCurrentPlayer($id);
            $currentPlayerIndex = 0;
            foreach ($players as $i => $player) {
                if ($player['idJ'] == $currentPlayer['idJ']) {
                    $currentPlayerIndex = $i;
                    break;
                }
            }
            $nextPlayerIndex = $currentPlayerIndex + 1;
            if ($nextPlayerIndex >= count($players)) {
                $nextPlayerIndex = 0;
                $numTour = getTourID($id);
                $numTour++;
                createNewTour($id);
                setCurrentPlayer($id, $players[0]['idJ']);
            } else {
                setCurrentPlayer($id, $players[$nextPlayerIndex]['idJ']);
            }
            $currentPlayer = getCurrentPlayer($id);

            //on fait avancer les pion au max
            $pions = fetchPions($id);
            $cartes = fetchCarteList($id);
            $numTour = getTourID($id) - 1;
            $plateau = fetchPlateau($id);
            foreach ($pions as $pion) {
                $idJ = $pion['idJ'];
                foreach ($cartes as $i => $carte) {
                    $id_carte = $carte['id_carte'];
                    $conditions = getValidationNeeded($id_carte);
                    $validated = true;
                    foreach ($conditions as $condition) {
                        $validated = $validated && isValidated($id, $numTour, $idJ, $id_carte);
                    }
                    if ($validated) {
                        if ($i == count($cartes) - 1) {
                            $win = $idJ;
                        } else {
                            $next_carte_id = $cartes[$i + 1]['id_carte'];
                            moovPion($plateau['id'], $pion['id'], $next_carte_id);
                        }
                    }
                }

            }
            break;
        case "chooseCard":
            $dices = getDiceValues($id);
            $cards = fetchCarteList($id);
            $idJ = getCurrentPlayer($id)['idJ'];
            $numTour = getTourID($id);
            foreach ($cards as $i => $card) {
                $dicesForCard = [];
                for ($k = 0; $k < count($dices); $k++) {
                    if ($_POST["dice" . $k] == $i) {
                        $dicesForCard[] = $dices[$k];
                    }
                }

                $conditions = getValidationNeeded($card["id_carte"]);
                foreach ($conditions as $j => $condition) {
                    $validated = false;
                    $coul = $condition["coul"];
                    switch ($condition["nom"]) {
                        case 'face_de_dé':
                            $val = $condition["faceVal"];
                            //TODO oral
                            for ($j = 0; $j < count($dicesForCard); $j++) {
                                if ($dicesForCard[$j]['couleur'] == $coul && $dicesForCard[$j]['valeur'] == $val) {
                                    $validated = true;
                                    break;
                                }
                            }
                            break;
                        case 'meme_au_choix':
                            $nb = $condition["memeNB"];
                            //validate if we have nb dice with the same value and the right color
                            $valuesCount = [];
                            foreach ($dicesForCard as $dice) {
                                if ($dice['couleur'] == $coul) {
                                    //TODO oral utilisation null check
                                    $valuesCount[$dice['valeur']] = ($valuesCount[$dice['valeur']] ?? 0) + 1;
                                }
                            }
                            //TODO oral smart guy
                            $validated = max($valuesCount) >= $nb;
                            break;
                        case 'seuil_de_dé':
                            $soeuil = $condition["soeuilSoeuil"];
                            $sens = $condition["soeuilSens"];
                            $sum = 0;
                            foreach ($dicesForCard as $dice) {
                                if ($dice['couleur'] == $coul) {
                                    $sum += $dice['valeur'];
                                }
                            }
                            switch ($sens) {
                                case '>':
                                    $validated = $sum > $soeuil;
                                    break;
                                case '<':
                                    $validated = $sum < $soeuil;
                                    break;
                            }
                            break;
                        case 'suite_au_choix':
                            $nb = $condition["suiteNB"];
                            $values = [];
                            //get only the values of the right color
                            foreach ($dicesForCard as $dice) {
                                if ($dice['couleur'] == $coul) {
                                    $values[] = $dice['valeur'];
                                }
                            }
                            //sort them and check if we have a suite of nb values
                            sort($values);
                            $current = 1;
                            $max = 1;
                            for ($j = 1; $j < count($values); $j++) {
                                if ($values[$j] == $values[$j - 1] + 1) {
                                    $current++;
                                    $max = max($max, $current);
                                } else if ($values[$j] == $values[$j - 1]) {
                                    continue;
                                } else {
                                    $current = 1;
                                }
                            }
                            $validated = $max >= $nb;
                            break;
                    }
                    validateCard($id, $numTour, $idJ, $card["id_carte"], $validated);
                }
            }
            cleanUpDices($id);
            $state = "choosedCard";
            break;

    }
}
$hasBegan = getTourID($id) !== null;
if ($hasBegan) {
    $alreadyChoosed = alreadyChoosenDice($id);
    if ($alreadyChoosed) {
        $state = "chosenDice";
        $dice = getDiceValues($id);
        if ($dice != []) {
            $blueValues = [];
            $yellowValues = [];
            $redValues = [];
            foreach ($dice as &$d) {
                switch ($d['couleur']) {
                    case 'bleu':
                        $blueValues[] = $d['valeur'];
                        break;
                    case 'jaune':
                        $yellowValues[] = $d['valeur'];
                        break;
                    case 'rouge':
                        $redValues[] = $d['valeur'];
                        break;
                }
            }
        }

        $numTour = getTourID($id);
        $remainingRolls = 3 - getLanceCount($id, $numTour, getCurrentPlayer($id)['idJ']);

    } else {
        $state = "chooseDice";
    }
}
//if we already choosed dices or not


$currentPlayer = getCurrentPlayer($id);
$cartes = fetchCarteList($id);
//modify the ids to be at fixed 3 digit length
foreach ($cartes as &$carte) {
    //TODO pour la présentation !
    $carte['id_carte'] = str_pad($carte['id_carte'], 3, '0', STR_PAD_LEFT);
}

$pions = fetchPions($id);
$players = fetchJoueurs($id);
//convert colors to english
//TODO this is pretty smart for oral
array_walk($players, function (&$joueur) {
    $colors = ['rouge' => 'red', 'bleu' => 'blue', 'vert' => 'green', 'jaune' => 'yellow'];
    $joueur['couleur'] = $colors[$joueur['couleur']] ?? $joueur['couleur'];
});
