<?php
global $connexion;

if (isset($_POST['action']) && $_POST["action"] == "createPartie") {

// Récupérer les données du formulaire de création de partie
    $nombre_cartes_plateau = $_POST['nombre_cartes_plateau'] ?? 0;
    $nombre_cartes_vertes = $_POST['nombre_cartes_vertes'] ?? 0;
    $nombre_cartes_oranges = $_POST['nombre_cartes_oranges'] ?? 0;
    $nombre_cartes_noires = $_POST['nombre_cartes_noires'] ?? 0;
    $ordre = $_POST['strategie_ordre'] ?? '';
    $strategie_ordre = 'ALEA'; // Default value
    $ordre = $_POST['strategie_ordre'] ?? '';

    if ($ordre === 'Honneur au plus jeune') {
        $strategie_ordre = '+JEUNE';
    } elseif ($ordre === 'Honneur au moins expérimenté') {
        $strategie_ordre = '-EXPE';
    }


// Insertion de la partie en base avec un état "À venir"
    $date = date("Y-m-d");
    $heure = date("H:i:s");
    $etat = "À venir";
// Création du plateau
    $query = $connexion->prepare("INSERT INTO plateau (nCarte) VALUES (?)");
    $query->bind_param("i", $nombre_cartes_plateau);
    $query->execute();


    $id_plateau = $connexion->insert_id;
    $query = $connexion->prepare("INSERT INTO partie (date, heure, etats, ordeDePassage,id_2, nbJoueurPartie) VALUES (?, ?, ?, ?, ?, 0)");
    $query->bind_param("ssssi", $date, $heure, $etat, $strategie_ordre, $id_plateau);
    $query->execute();

    $id_partie = $connexion->insert_id;


// Sélection aléatoire des cartes vertes, oranges et noires
    $cartes_vertes = array();
    $cartes_oranges = array();
    $cartes_noires = array();

    $vertes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 58, 61, 72];
    $oranges = [31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 56, 59, 62, 64, 66, 68, 70, 71];
    $noires = [57, 60, 63, 65, 67, 69, 73, 74, 75];

// Remplir les tableaux $cartes_vertes, $cartes_oranges, $cartes_noires avec les IDs de cartes aléatoires
// Sélection aléatoire des cartes vertes
    if ($nombre_cartes_vertes > 0) {
        shuffle($vertes); // Mélanger le tableau $vertes
        $indexes_vertes = array_slice($vertes, 0, min($nombre_cartes_vertes, count($vertes))); // Sélectionner les premiers indices requis
        foreach ($indexes_vertes as $index) {
            $cartes_vertes[] = $index;
        }
    }

// Sélection aléatoire des cartes oranges
    if ($nombre_cartes_oranges > 0) {
        shuffle($oranges); // Mélanger le tableau $oranges
        $indexes_oranges = array_slice($oranges, 0, min($nombre_cartes_oranges, count($oranges))); // Sélectionner les premiers indices requis
        foreach ($indexes_oranges as $index) {
            $cartes_oranges[] = $index;
        }
    }

// Sélection aléatoire des cartes noires
    if ($nombre_cartes_noires > 0) {
        shuffle($noires); // Mélanger le tableau $noires
        $indexes_noires = array_slice($noires, 0, min($nombre_cartes_noires, count($noires))); // Sélectionner les premiers indices requis
        foreach ($indexes_noires as $index) {
            $cartes_noires[] = $index;
        }
    }


    echo "Cartes vertes :";
    print_r($cartes_vertes);

    echo "Cartes oranges :";
    print_r($cartes_oranges);

    echo "Cartes noires :";
    print_r($cartes_noires);

// Insérer les cartes sélectionnées dans la table estCompose avec l'ordre
    $ordre = 1; // Initialisation de l'ordre
    foreach (array_merge($cartes_vertes, $cartes_oranges, $cartes_noires) as $carte) {
        if (!empty($carte)) {
            $query = $connexion->prepare("INSERT INTO estCompose (id_plateau, id_carte, ordre) VALUES (?, ?, ?)");
            $query->bind_param("iii", $id_plateau, $carte, $ordre);
            $query->execute();
            $ordre++; // Incrémenter l'ordre pour la prochaine carte
        }
    }


    // Afficher le formulaire de sélection des joueurs
    // ...
    echo "cest bon";
    // Rediriger l'utilisateur vers la page de sélection des joueurs
    header("Location: index.php?page=selectionJoueurs&id_partie=$id_partie");

}
