<?php
function request1(): array
{
    global $connexion;
    $sql1 = "SELECT 
                             (SELECT COUNT(*) FROM joueur) AS nb_joueurs,
                             (SELECT COUNT(*) FROM equipe) AS nb_equipes,
                             (SELECT COUNT(*) FROM classement) AS nb_classements,
                             (SELECT COUNT(*) FROM tournoi) AS nb_tournois
                         FROM dual";
    $result1 = $connexion->query($sql1);

    $sql2 = "SELECT AVG(nb_participants) AS moyenne_participants
             FROM (
               SELECT COUNT(idJ) AS nb_participants
               FROM participe
               GROUP BY idT
             ) AS participants";
    $result2 = $connexion->query($sql2);

    if ($result1 && $result2) {
        $row1 = $result1->fetch_assoc();
        $row2 = $result2->fetch_assoc();
        return [
            "nb_joueurs" => $row1["nb_joueurs"],
            "nb_equipes" => $row1["nb_equipes"],
            "nb_classements" => $row1["nb_classements"],
            "nb_tournois" => $row1["nb_tournois"],
            "moyenne_participants_par_tournoi" => $row2["moyenne_participants"]
        ];
    } else {
        return [];
    }
}

function request2(): array
{
    global $connexion;
    $sql = "SELECT tournoi.nom, tournoi.dateDeb, phase.niveaux
                        FROM tournoi
                        JOIN phase ON tournoi.idT = phase.idT
                        JOIN participe ON tournoi.idT = participe.idT AND phase.niveaux = participe.niveau
                        JOIN joueur ON participe.idJ = joueur.idJ
                        WHERE joueur.pseudo = 'nom_utilisateur'
                        ORDER BY tournoi.dateDeb DESC, phase.niveaux DESC";
    $result = $connexion->query($sql);
    if ($result->num_rows > 0) {
        $rows = [];
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    } else {
        return [];
    }
}

function request3(): int
{
    global $connexion;
    $sql = "SELECT COUNT(*)
                         FROM classementEquipe
                         WHERE idC IN (
                           SELECT classementEquipe.idC
                           FROM classementEquipe
                           LEFT JOIN sontClasses ON classementEquipe.idC = sontClasses.idC
                           LEFT JOIN classementIndividuel ON sontClasses.idE = classementIndividuel.idC
                           JOIN estClasse ON classementIndividuel.idc = estClasse.idc
                           GROUP BY classementEquipe.idC
                           HAVING MAX(estClasse.rang) > 1 OR MAX(estClasse.rang) IS NULL
                         )";

    $result = $connexion->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        return $row["COUNT(*)"];
    } else {
        return -1;
    }
}

function request4(): array
{
    global $connexion;
    $sql = "SELECT YEAR(tournoi.dateDeb) AS annee, AVG(nb_participants) AS moyenne_participants
                         FROM tournoi
                         LEFT JOIN (
                           SELECT idT, COUNT(idJ) AS nb_participants
                           FROM participe
                           GROUP BY idT
                         ) AS participants ON tournoi.idT = participants.idT
                         WHERE tournoi.dateDeb >= DATE_SUB(CURDATE(), INTERVAL 3 YEAR)
                         GROUP BY YEAR(tournoi.dateDeb)";

    $result = $connexion->query($sql);

    if ($result->num_rows > 0) {
        $return = [];
        while ($row = $result->fetch_assoc()) {
            $return[] = $row;
        }
        return $return;
    } else {
        return [];
    }

}

function request5(): array
{
    global $connexion;
    $sql = "SELECT joueur.nom, joueur.prenom 
            FROM joueur 
            JOIN estClasse ON joueur.idJ = estClasse.idJ 
            JOIN classement ON estClasse.idC = classement.idC 
            JOIN classementIndividuel ON classement.idc = classementIndividuel.idc 
            WHERE classement.portee = 'nationale' 
            GROUP BY joueur.idJ, joueur.nom, joueur.prenom 
            HAVING COUNT(*) >= 2 
            ORDER BY joueur.nom, joueur.prenom";

    $result = $connexion->query($sql);
    if ($result->num_rows > 0) {
        $return = [];
        while ($row = $result->fetch_assoc()) {
            $return[] = $row;
        }
        return $return;
    } else {
        return [];
    }
}

function request6(): array
{
    global $connexion;
    $sql = "SELECT plateau.nCarte, COUNT(partie.id) AS nombre_de_parties
                         FROM partie
                         JOIN plateau ON partie.id_2 = plateau.id
                         GROUP BY plateau.nCarte";

    $result = $connexion->query($sql);
    if ($result->num_rows > 0) {
        $return = [];
        while ($row = $result->fetch_assoc()) {
            $return[] = $row;
        }
        return $return;
    } else {
        return [];
    }
}

function request7(): array
{
    global $connexion;
    $sql = "SELECT joueur.pseudo, COUNT(joue.id) AS nombre_de_parties
                FROM joue
                JOIN joueur ON joue.idJ = joueur.idJ
                GROUP BY joueur.idJ
                ORDER BY nombre_de_parties DESC
                LIMIT 5";
    $result = $connexion->query($sql);
    if ($result->num_rows > 0) {
        $return = [];
        while ($row = $result->fetch_assoc()) {
            $return[] = $row;
        }
        return $return;
    } else {
        return [];
    }
}