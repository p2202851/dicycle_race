<?php


function fetchCarteList(int $id): array
{
    global $connexion;
    $query = $connexion->prepare("SELECT id_carte, ordre 
FROM partie 
    JOIN plateau ON partie.id_2 = plateau.id 
    LEFT JOIN estCompose ON plateau.id = estCompose.id_plateau  
WHERE partie.id = ?
ORDER BY ordre");
    $query->bind_param('i', $id);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function fetchPlateau(int $id): array
{
    global $connexion;
    $query = $connexion->prepare("SELECT plateau.* FROM partie JOIN plateau ON partie.id_2 = plateau.id WHERE partie.id = ?");
    $query->bind_param('i', $id);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_all(MYSQLI_ASSOC)[0];
}

function fetchPions(int $pionId): array
{
    global $connexion;
    $query = $connexion->prepare("SELECT pion.id FROM partie
    JOIN plateau ON partie.id_2 = plateau.id
    JOIN pion ON plateau.id = pion.id_2
WHERE partie.id = ?");
    $query->bind_param('i', $pionId);
    $query->execute();
    $result = $query->get_result();
    $ids = $result->fetch_all(MYSQLI_ASSOC);
    $pions = [];
    foreach ($ids as $pionId) {
        $query2 = $connexion->prepare("SELECT
    couleur,
    IF(pion.id_3 IS NULL, 0, ordre) as ordre,
    pion.id,
    pion.idJ
FROM pion
    LEFT JOIN plateau on pion.id_2 = plateau.id
    LEFT JOIN estCompose on pion.id_3 = estCompose.id_carte AND plateau.id = estCompose.id_plateau
WHERE pion.id = ?");
        $query2->bind_param('i', $pionId['id']);
        $query2->execute();
        $result = $query2->get_result();
        $pions[] = $result->fetch_all(MYSQLI_ASSOC)[0];
    }
    return $pions;
}

function fetchJoueurs(int $id): array
{
    global $connexion;
    $query = $connexion->prepare("SELECT ordeDePassage FROM partie WHERE id=?");
    $query->bind_param('i', $id);
    $query->execute();
    $result = $query->get_result();
    switch ($result->fetch_all(MYSQLI_ASSOC)[0]['ordeDePassage']) {
        case "ALEA":
            $query = $connexion->prepare("SELECT couleur, pseudo, joueur.idJ FROM partie JOIN plateau ON partie.id_2 = plateau.id LEFT JOIN estCompose ON id_plateau = id JOIN pion ON id_3 = estCompose.id_carte LEFT JOIN joueur ON joueur.idJ = pion.idJ WHERE partie.id = ?");
            break;
        case "-EXPE":
            $query = $connexion->prepare("SELECT couleur, pseudo, joueur.idJ FROM partie JOIN plateau ON partie.id_2 = plateau.id LEFT JOIN estCompose ON id_plateau = id JOIN pion ON id_3 = estCompose.id_carte JOIN joueur WHERE partie.id = ?");
            break;
        case "+JEUNE":
            $query = $connexion->prepare("
                   SELECT couleur, pseudo, joueur.idJ FROM partie
                                JOIN plateau ON partie.id_2 = plateau.id
                                JOIN pion ON plateau.id = pion.id_2
                                JOIN joueur ON joueur.idJ = pion.idJ
                                LEFT JOIN (
    SELECT joueur.idJ, COUNT(joue.id) AS nombre_de_parties_jouees
    FROM joueur
             LEFT JOIN joue ON joueur.idJ = joue.idJ
    GROUP BY joueur.idJ
) AS parties_jouees ON parties_jouees.idJ = joueur.idJ
WHERE partie.id = ?
ORDER BY parties_jouees.nombre_de_parties_jouees");
            break;
    }
    $query->bind_param('i', $id);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function fetchOrdrePassage(int $id): string
{
    global $connexion;
    $query = $connexion->prepare("SELECT ordeDePassage FROM partie WHERE id=?");
    $query->bind_param('i', $id);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_all(MYSQLI_ASSOC)[0]['ordrePassage'];
}

function saveBegin(int $id, $date, $heure)
{
    global $connexion;
    $query = $connexion->prepare("UPDATE partie SET date = ? , heure = ? WHERE id = ?");
    $query->bind_param('ssi', $date, $heure, $id);
    $query->execute();
}


function setCurrentPlayer(int $id, int $idJ): void
{
    global $connexion;
    $query = $connexion->prepare("UPDATE partie SET joueurEnCour = ? WHERE id = ?");
    $query->bind_param('ii', $idJ, $id);
    $query->execute();
}

function getCurrentPlayer(int $id): array
{
    global $connexion;
    $query = $connexion->prepare("SELECT pseudo, idJ FROM partie JOIN joueur ON partie.joueurEnCour = joueur.idJ WHERE partie.id = ?");
    $query->bind_param('i', $id);
    $query->execute();
    $result = $query->get_result();
    $data = $result->fetch_all(MYSQLI_ASSOC);
    if ($data == []) {
        return [];
    }
    return $data[0];
}

function getTourID(int $id): int|null
{
    global $connexion;
    $query = $connexion->prepare("SELECT numT FROM tour WHERE id = ? ORDER BY numT DESC LIMIT 1");
    $query->bind_param('i', $id);
    $query->execute();
    $result = $query->get_result();
    $data = $result->fetch_all(MYSQLI_ASSOC);
    if ($data == []) {
        return null;
    }
    return $data[0]['numT'];
}

function createNewTour($id): void
{
    global $connexion;
    $query = $connexion->prepare("INSERT INTO tour (id, numT) VALUES (?, ?)");
    $tourCount = (getTourID($id) + 1) ?? 0;
    $query->bind_param('ii', $id, $tourCount);
    $query->execute();
}

function fetchPlayerCard($id, $idJ): array
{
    global $connexion;
    $query = $connexion->prepare("SELECT carte.* FROM partie JOIN joueur JOIN pion using(idJ) JOIN carte ON carte.id = pion.id_3 WHERE joueur.idJ = ? AND partie.id = ?");
    $query->bind_param('ii', $idJ, $id);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_all(MYSQLI_ASSOC)[0];
}

function saveMain(int $id, int $blueCount, int $yellowCount, int $redCount): void
{
    global $connexion;
    $idJ = getCurrentPlayer($id)['idJ'];
    $numT = getTourID($id);
    $query = $connexion->prepare("INSERT INTO choisitMain (idP, id_carte, numT, idJ, nbDeRouge, nbDeJaune, nbDeBleu) VALUES (?, ?, ?, ?, ?, ?, ?)");
    $id_carte = fetchPlayerCard($id, $idJ)['id'];
    $query->bind_param('iiiiiii', $id, $id_carte, $numT, $idJ, $redCount, $yellowCount, $blueCount);
    $query->execute();
}

function getMain(int $id, int $idJ): array
{
    global $connexion;
    //fetch numT
    $numT = getTourID($id);
    $query = $connexion->prepare("SELECT * FROM choisitMain WHERE idP = ? AND numT = ? AND idJ = ?");
    $query->bind_param('iii', $id, $numT, $idJ);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_all(MYSQLI_ASSOC)[0];
}

function getLanceCount(int $id, int $numT, $idJ): int
{
    global $connexion;
    if ($idJ == null) {
        $query = $connexion->prepare("SELECT COUNT(*) as count FROM lancer WHERE id = ? AND numT = ?");
        $query->bind_param('ii', $id, $numT);
    } else {
        $query = $connexion->prepare("SELECT COUNT(*) as count FROM effectue WHERE id = ? AND numT = ? AND idJ = ?");
        $query->bind_param('iii', $id, $numT, $idJ);
    }
    $query->execute();
    $result = $query->get_result();
    $data = $result->fetch_all(MYSQLI_ASSOC);
    return $data[0]['count'] == null ? 0 : $data[0]['count'];
}

function saveDiceValues($id, $blueValues, $yellowValues, $redValues): void
{
    global $connexion;
    $idJ = getCurrentPlayer($id)['idJ'];
    $numT = getTourID($id);
    $numLance = getLanceCount($id, $numT, null) + 1;
    $id_carte = fetchPlayerCard($id, $idJ)['id'];
    $rang = 0;
    $queryString = "INSERT INTO deLance (rang, couleur, valeur, id, numT, idL) VALUES (?, ?, ?, ?, ?, ?)";
    $couleur = "bleu";

    $query = $connexion->prepare("INSERT INTO lancer (id, numT, idL ) VALUES (?, ?, ?)");
    $query->bind_param('iii', $id, $numT, $numLance);
    $query->execute();
    $query = $connexion->prepare("INSERT INTO effectue (idJ, id, numT, idL) VALUES (?, ?, ?, ?)");
    $query->bind_param('iiii', $idJ, $id, $numT, $numLance);
    $query->execute();

    foreach ($blueValues as $value) {
        $query = $connexion->prepare($queryString);
        $query->bind_param('isiiii', $rang, $couleur, $value, $id, $numT, $numLance);
        $query->execute();
        $rang++;
    }
    $couleur = "jaune";
    foreach ($yellowValues as $value) {
        $query = $connexion->prepare($queryString);
        $query->bind_param('isiiii', $rang, $couleur, $value, $id, $numT, $numLance);
        $query->execute();
        $rang++;
    }
    $couleur = "rouge";
    foreach ($redValues as $value) {
        $query = $connexion->prepare($queryString);
        $query->bind_param('isiiii', $rang, $couleur, $value, $id, $numT, $numLance);
        $query->execute();
        $rang++;
    }
}

function getDiceValues(int $id): array
{
    global $connexion;
    $numT = getTourID($id);
    $idJ = getCurrentPlayer($id)['idJ'];
    $query = $connexion->prepare("
SELECT couleur, valeur, rang 
FROM deLance
JOIN lancer on deLance.id = lancer.id and deLance.numT = lancer.numT and deLance.idL = lancer.idL
JOIN effectue on lancer.id = effectue.id and lancer.numT = effectue.numT and lancer.idL = effectue.idL
WHERE
    deLance.id = ? AND
    deLance.numT = ?
    AND effectue.idJ = ?
  AND
    effectue.idL = (
        SELECT MAX(idL)
        FROM effectue
        WHERE id = ?
          AND numT = ?
    )
");
    $query->bind_param('iiiii', $id, $numT, $idJ, $id, $numT);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function cleanUpDices(int $id): void
{
    global $connexion;
    $numT = getTourID($id);
    $idJ = getCurrentPlayer($id)['idJ'];
    $query = $connexion->prepare("DELETE FROM deLance WHERE id = ? AND numT = ? AND idL = (SELECT MAX(idL) FROM effectue WHERE id = ? AND numT = ?)");
    $query->bind_param('iiii', $id, $numT, $id, $numT);
    $query->execute();
}

function alreadyChoosenDice(int $id): bool
{
    global $connexion;
    $numT = getTourID($id);
    $idJ = getCurrentPlayer($id)['idJ'];
    $query = $connexion->prepare("SELECT * FROM choisitMain WHERE idP = ? AND numT = ? AND idJ = ?");
    $query->bind_param('iii', $id, $numT, $idJ);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_all(MYSQLI_ASSOC) != [];

}

function fetchPartieId()
{
    global $connexion;
    $query = $connexion->prepare("SELECT MAX(id) FROM partie");
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_all(MYSQLI_ASSOC)[0]['MAX(id)'];

}

function getValidationNeeded($idCard): array
{
    global $connexion;
    $query = $connexion->prepare("
SELECT 
    nom,
    coul,
    lMA.nb as memeNB,
    sAC.nb as suiteNB,
    val as faceVal,
    seuil as soeuilSoeuil,
    sens as soeuilSens
FROM carte
    LEFT JOIN app.contrain c on carte.id = c.id_2
    LEFT JOIN app.contrainte as c2 ON c.id_1 = c2.id
    LEFT JOIN app.suiteAuChoix sAC on c2.id = sAC.id_1
    LEFT JOIN app.faceDeDes fDD on c2.id = fDD.id_1
    LEFT JOIN app.lesMemesAuchoix lMA on c2.id = lMA.id_1
    LEFT JOIN app.soeuiDeDes sDD on c2.id = sDD.id_1
        WHERE carte.id = ?
");
    $query->bind_param('i', $idCard);
    $query->execute();
    $result = $query->get_result();
    return $result->fetch_all(MYSQLI_ASSOC);
}

function getConditionsWithState($idCard, $idJ): array
{
    global $connexion;
    $conditions = getValidationNeeded($idCard);
    $result = [];
    foreach ($conditions as $condition) {
        $result[] = [
            "nom" => $condition['nom'],
            "coul" => $condition['coul'],
            "validated" => isValidated($idCard, $condition['numT'], $idJ, $condition['id_1'])
        ];
    }
}

function isValidated($id, $numT, $idJ, $idC): bool
{
    global $connexion;
    $query = $connexion->prepare("SELECT validee FROM tenteValidation WHERE id_1 = ? AND numT = ? AND id_2 = ? AND idJ = ?");
    $query->bind_param('iiii', $id, $numT, $idC, $idJ);
    $query->execute();
    $result = $query->get_result();
    return ($result->fetch_all(MYSQLI_ASSOC)[0]['validee'] ?? "ko") == "ok";
}

function validateCard($id, $numT, $idJ, $idC, $ok): void
{
    global $connexion;
    $query = $connexion->prepare("SELECT nbTentative FROM tenteValidation WHERE id_1 = ? AND numT = ? AND id_2 = ? AND idJ = ?");
    $query->bind_param('iiii', $id, $numT, $idC, $idJ);
    $query->execute();
    $result = $query->get_result();
    $nbTentative = ($result->fetch_all(MYSQLI_ASSOC)[0]['nbTentative'] ?? 0) + 1;
    //TODO check the line over which probably does not get the result correctly
    if ($nbTentative == 1) {
        $query = $connexion->prepare("INSERT INTO tenteValidation (id_1, numT, id_2, idJ, nbTentative, validee)
VALUE (?, ?, ?, ?, ?, ?)
");
        $validee = $ok ? "ok" : "ko";
        $query->bind_param('iiiiis', $id, $numT, $idC, $idJ, $nbTentative, $validee);
        $query->execute();
    } else {
        $query = $connexion->prepare("UPDATE tenteValidation SET validee = ?, nbTentative = ? WHERE id_1 = ? AND numT = ? AND id_2 = ? AND idJ = ?");
        $validee = $ok ? "ok" : "ko";
        $query->bind_param('siiiii', $validee, $nbTentative, $id, $numT, $idC, $idJ);
        $query->execute();

    }

}

//function fetchRang($id, $idJ, $numT): int
//{
//    global $connexion;
//    $query = $connexion->prepare("SELECT MAX(rang) FROM effectue WHERE id = ? AND idJ = ? AND numT = ?");
//    $query->bind_param('iii', $id, $idJ, $numT);
//    $query->execute();
//    $result = $query->get_result();
//    return $result->fetch_all(MYSQLI_ASSOC)[0]['MAX(rang)'];
//
//}

function everyOneToFirstCase(int $id): void
{
    global $connexion;
    //fetch the first card id
    $query = $connexion->prepare("SELECT id_carte FROM estCompose WHERE id_plateau = (SELECT id_2 FROM partie WHERE id = ?) GROUP BY id_carte ORDER BY MIN(ordre) LIMIT 1");
    $query->bind_param('i', $id);
    $query->execute();
    $result = $query->get_result();
    $idCarte = $result->fetch_all(MYSQLI_ASSOC)[0]['id_carte'];
    //set the pion to be on the first card
    $query = $connexion->prepare("UPDATE pion SET id_3 = ? WHERE id_2 = (SELECT id_2 FROM partie WHERE id = ?)");
    $query->bind_param('ii', $idCarte, $id);
    $query->execute();
}

function moovPion(int $idPlateau, int $idPion, int $idCarte): void
{
    global $connexion;
    $query = $connexion->prepare("UPDATE pion SET id_3 = ? WHERE id_2 = ? AND id = ?");
    $query->bind_param('iii', $idCarte, $idPlateau, $idPion);
    $query->execute();
}