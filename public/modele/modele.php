<?php

/**
 * Initialise la connexion à la base de données courante (spécifiée selon constante
 *    globale SERVEUR, UTILISATEUR, MOTDEPASSE, BDD)
 */
function open_connection_DB(): void
{
    global $connexion;

    $connexion = mysqli_connect(SERVEUR, UTILISATEUR, MOTDEPASSE, BDD);
    if (mysqli_connect_errno()) {
        printf("Échec de la connexion : %s\n", mysqli_connect_error());
        exit();
    }
}

/**
 *    Ferme la connexion courante
 * */
function close_connection_DB(): void
{
    global $connexion;

    mysqli_close($connexion);
}