<?php
//mode could be all, recent or fast
function fetchPartiesList(bool $ended, string $mode): array
{
    global $connexion;

    if ($ended) {
        $state = "WHERE etats = 'T'";
    } else {
        $state = "WHERE etats = \"À venir\"";
    }

    if ($mode == "recent") {
        $request = $connexion->prepare('SELECT partie.id FROM partie ' . $state . ' ORDER BY date DESC, heure DESC LIMIT 50');
    } else if ($mode == "fast") {
        $request = $connexion->prepare('SELECT partie.id FROM partie ' . $state . ' ORDER BY duree DESC LIMIT 50');
    } else {
        $request = $connexion->prepare('SELECT partie.id FROM partie ' . $state);
    }

    $return = [];
    $request->execute();
    $result = $request->get_result();
    while ($row = $result->fetch_assoc()) {
        $return[] = $row['id'];
    }
    return $return;
}


//mode could be all, recent or fast
function fetchPartieDetails(int $id): array
{
    global $connexion;

    $request = $connexion->prepare("SELECT * FROM partie LEFT JOIN plateau ON partie.id_2 = plateau.id WHERE partie.id = " . $id . ";");
    $request->execute();
    $result = $request->get_result();


    $row = $result->fetch_assoc();
    //add the id of the game to the result
    $return = [
        "id" => $row['id'],
        "date" => $row['date'],
        "heure" => $row['heure'],
        "nCarte" => $row["nCarte"],
        "count" => [],
        "pseudo" => [],
    ];
    $request = $connexion->prepare("SELECT carte.niveau, COUNT(carte.niveau) as count FROM carte JOIN estCompose ON carte.id = estCompose.id_carte WHERE estCompose.id_plateau = " . $row["id"] . " GROUP BY carte.niveau;");
    $request->execute();
    $result = $request->get_result();

    while ($row2 = $result->fetch_assoc()) {
        $return[$row2["niveau"]] = $row2["count"];
    }

    $request = $connexion->prepare("SELECT joueur.pseudo, joue.score FROM joueur JOIN joue ON joueur.idJ = joue.idJ WHERE joue.id = " . $row["id"] . " GROUP BY joueur.nom, joueur.prenom, joueur.pseudo, joue.score ORDER BY joue.score");
    $request->execute();
    $result = $request->get_result();
    $count = 0;
    while ($row2 = $result->fetch_assoc()) {
        $return["joueurs"][] = [$row2["pseudo"], $row2["score"]];
        if ($count < 3) {
            $return["podium"][] = [$row2["pseudo"], $row2["score"]];
        }
        $count++;
    }
    return $return;
}