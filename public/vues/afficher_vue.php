<?php
global $endedIds, $inGameIds, $details;
function formatPartieId(string $id): string
{
    return "<a href='index.php?page=afficher&id=" . $id . "'>" . $id . "</a>";
}

?>

<link rel="stylesheet" href="css/afficher.css">

<div class="selector">
    <form action="index.php?page=afficher" method="post">
        <label for="mode">Mode d'affichage</label>
        <select name="mode" id="mode">
            <option value="all" <?php if (isset($_POST["mode"]) && $_POST['mode'] == 'all') echo 'selected'; ?>>Toutes
                les
                parties
            </option>
            <option value="recent" <?php if (isset($_POST["mode"]) && $_POST['mode'] == 'recent') echo 'selected'; ?>>
                Les 50
                parties les plus
                récentes
            </option>
            <option value="fast" <?php if (isset($_POST["mode"]) && $_POST['mode'] == 'fast') echo 'selected'; ?>>Les 50
                parties plus rapides par
                taille de plateau
            </option>
        </select>
        <input type="submit" value="Valider">
    </form>
</div>

<div class="details">

    <?php

    if ($details != null) {
        // display the data as just a list
        echo "<ul>";
        echo "<li>" . "Identifiant de partie: " . $details['id'] . "</li>";
        echo "<li>Date et horaire de la partie: " . $details['date'] . " " . $details['heure'] . "</li>";
        echo "<li>Taille du plateau: " . $details["nCarte"] . "</li>";
        //if orange, noir, verte in the array
        if (isset($details["verte"])) echo "<li>nVerte " . $details["verte"] . "</li>";
        if (isset($details["orange"])) echo "<li>nOrange " . $details["orange"] . "</li>";
        if (isset($details["noire"])) echo "<li>nNoire " . $details["noire"] . "</li>";
        if (isset($details["joueurs"])) {
            echo "<li>Liste des pseudos des joueurs: ";
            echo implode(", ", array_column($details["joueurs"], 0));
            echo "</li>";
        }
        if (isset($details["podium"])) {
            echo "<li>Podium: ";
            echo "<ul>";
            foreach ($details["podium"] as $podium) {
                echo "<li>" . $podium[0] . " avec un score de " . $podium[1] . "</li>";
            }
            echo "</ul>";
            echo "</li>";
        }
        echo "</ul>";

    }
    ?>
</div>


<div class="links">


    <h2>Partie A venir</h2>
    <?php
    foreach ($endedIds as $endedId) {
        echo formatPartieId($endedId) . "<br>";
    } ?>

    <h2>Partie en cours</h2>
    <?php
    foreach ($inGameIds as $inGameId) {
        echo formatPartieId($inGameId) . "<br>";
    } ?>
</div>


