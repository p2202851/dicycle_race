<?php
global $id_partie;
global $joueurs;
global $result_partie_joueurs;
?>
<link rel="stylesheet" href="css/selection.css">
<div>
<form method="POST" action="index.php?page=selectionJoueurs&id_partie=<?php echo $id_partie; ?>">
    <label for="joueur">Sélectionnez un joueur :</label>
    <select name="joueur">
        <?php while ($row = $joueurs->fetch_assoc()) { ?>
            <option value="<?php echo $row['idJ']; ?>"><?php echo $row['nom'] . ' ' . $row['prenom'] . ' (' . $row['pseudo'] . ')'; ?></option>
        <?php } ?>
    </select>
    <input id="ajouter" type="submit" name="ajouter_joueur" value="Ajouter Joueur">
</form>
<form method="POST" action="index.php?page=selectionJoueurs&id_partie=<?php echo $id_partie; ?>">
    <?php if ($result_partie_joueurs) {
        if ($result_partie_joueurs->num_rows > 0) { ?>
            <select name="joueur_retirer">
                <?php while ($row_joueur = $result_partie_joueurs->fetch_assoc()) { ?>
                    <option value="<?php echo $row_joueur['idJ']; ?>"><?php echo $row_joueur['nom'] . ' ' . $row_joueur['prenom'] . ' (' . $row_joueur['pseudo'] . ')'; ?></option>
                <?php } ?>
            </select>

            <input id="retirer" type="submit" name="retirer_joueur" value="Retirer Joueur">

        <?php } else { ?>
            Aucun joueur n'a été ajouté à cette partie.
        <?php }
    } ?>


</form></div>