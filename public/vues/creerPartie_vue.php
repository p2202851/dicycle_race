<link rel="stylesheet" href="css/creer.css">
<div class="creer">
<h1>Créer une nouvelle partie</h1>
<form method="POST" action="index.php?page=creerPartie">
    <label for="nombre_cartes_plateau">Nombre de cartes sur le plateau :</label>
    <label>
        <input type="number" name="nombre_cartes_plateau" required>
    </label><br>
    <label for="nombre_cartes_vertes">Nombre de cartes vertes à sélectionner :</label>
    <label>
        <input type="number" name="nombre_cartes_vertes" required>
    </label><br>
    <label for="nombre_cartes_oranges">Nombre de cartes oranges à sélectionner :</label>
    <label>
        <input type="number" name="nombre_cartes_oranges" required>
    </label><br>
    <label for="nombre_cartes_noires">Nombre de cartes noires à sélectionner :</label>
    <label>
        <input type="number" name="nombre_cartes_noires" required>
    </label><br>
    <label for="strategie_ordre">Sélectionnez la stratégie d\'ordre de passage :</label>
    <select name="strategie_ordre">
        <option value="Honneur au plus jeune">Honneur au plus jeune</option>
        <option value="Honneur au moins expérimenté">Honneur au moins expérimenté</option>
        <option value="Aléatoire">Aléatoire</option>
    </select>
    <input type="hidden" name="action" value="createPartie">
    <input type="submit" value="Créer la partie">
</form> </div>