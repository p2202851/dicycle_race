<link rel="stylesheet" href="css/jouer.css">


<?php
global $cartes;
global $pions;
global $players;

echo "<div class=\"carteImg\">";

echo "<div id=\"carteDepart\">";

echo "<img src='img/cartes/CDepart.png' alt='carte' id='carteDepartArrive' />";
echo "<div class='pionContainer'>";
foreach ($pions as $pion) {
    if ($pion['ordre'] == 0) {
        echo "<img src='img/elements_graphiques/pion_" . $pion['couleur'] . ".png' alt='pion' class='pion'/>";
    }
}
echo "</div>";
echo "</div>";
foreach ($cartes as &$carte) {
    echo "<div id=\"carte" . $carte['id_carte'] . "\">";
    echo "<p>" . $carte['ordre'] . "</p>";
    echo "<img src='img/cartes/C" . $carte['id_carte'] . ".png' alt='carte' />";
    echo "<div class='pionContainer'>";

    foreach ($pions as $pion) {
        if ($pion['ordre'] == $carte['ordre']) {
            echo "<img src='img/elements_graphiques/pion_" . $pion['couleur'] . ".png' alt='pion' class='pion'/>";
        }
    }
    echo "</div>";

    echo "</div>";
}

echo "<div id=\"carteArrivee\">";
echo "<img src='img/cartes/CArrivee.png' alt='carte' id='carteDepartArrive'/>";
echo "<div class='pionContainer'>";
foreach ($pions as $pion) {
    if ($pion['ordre'] == -1) {
        echo "<img src='img/elements_graphiques/pion_" . $pion['couleur'] . ".png' alt='pion' class='pion'/>";
    }
}
echo "</div>";
echo "</div>";

echo "</div>";

global $win;
if (isset($win)) {
    echo "<p>Le joueur " . $win . " a gagné</p>";
} else {


    echo "<div class='player'>";
    echo "<h2>Joueurs</h2>";
    global $currentPlayer;
    foreach ($players as $player) {
        if ($currentPlayer != null && $currentPlayer['idJ'] == $player['idJ']) {
            echo "<p style='color:" . $player['couleur'] . "'>" . $player['pseudo'] . " (c'est à toi)</p>";
        } else {
            echo "<p style='color:" . $player['couleur'] . "'>" . $player['pseudo'] . "</p>";
        }
    }
    echo "</div>";

    global $hasBegan;
    if (!$hasBegan) {
        echo '
<!-- start the game-->
<form action="index.php?page=jouer" method="post" class="tourButton">
    <button name="action" value="start">start</button>
</form>
';
    } else {
        global $state;
        global $blueValues;
        global $yellowValues;
        global $redValues;
        switch ($state) {
            case 'rolledDice':
            case "chosenDice":
                global $numTour;
                global $remainingRolls;
                echo "<p>tour n°" . $numTour . "</p>";
                echo "<p>lancers restants : " . $remainingRolls . "</p>";

                echo "<form action='index.php?page=jouer' method='post' class='diceForm'>";
                global $cartes;
                $numberOfCartes = count($cartes);
                $cardSelection = "";
                for ($i = 0; $i < $numberOfCartes; $i++) {
                    $cardSelection .= "<option value='" . $i . "'>Carte " . $i . "</option>";
                }
                $cardSelection .= "</select>";
                $diceCount = 0;
                if (isset($blueValues)) {
                    foreach ($blueValues as $blueValue) {
                        echo "<div class='dice'>";
                        echo "<img src='img/elements_graphiques/deB" . $blueValue . ".png' alt='de' />";
                        echo "<select name='dice" . $diceCount . "'>";
                        echo $cardSelection;
                        echo "</div>";
                        $diceCount++;
                    }
                }
                if (isset($yellowValues)) {
                    foreach ($yellowValues as $yellowValue) {
                        echo "<div class='dice'>";
                        echo "<img src='img/elements_graphiques/deJ" . $yellowValue . ".png' alt='de' />";
                        echo "<select name='dice" . $diceCount . "'>";
                        echo $cardSelection;
                        echo "</div>";
                        $diceCount++;
                    }
                }
                if (isset($redValues)) {
                    foreach ($redValues as $redValue) {
                        echo "<div class='dice'>";
                        echo "<img src='img/elements_graphiques/deR" . $redValue . ".png' alt='de' />";
                        echo "<select name='dice" . $diceCount . "'>";
                        echo $cardSelection;
                        echo "</div>";
                        $diceCount++;
                    }
                }

                if (!isset($blueValues) && !isset($yellowValues) && !isset($redValues) && $remainingRolls > 0) {
                    echo "<form action='index.php?page=jouer' method='post' class='tourButton'>
                <button name='action' value='roll'>Lancer les dés</button>
            </form>
            ";
                } else if ((isset($blueValues) || isset($yellowValues) || isset($redValues)) && $remainingRolls >= 0) {
                    echo "<form action='index.php?page=jouer' method='post' class='tourButton'>
                <button name='action' value='chooseCard'>Choisir les cartes</button>";
                    echo "</form>";
                } else {
                    echo "<form action='index.php?page=jouer' method='post' class='tourButton'>
                <button name='action' value='endTour'>Terminer le tour</button>";
                    echo "</form>";
                }

                break;
            case "chooseDice":
                echo '
    <form action="index.php?page=jouer" method="post" class="tourButton">
        <label for="blue">Nombre de dés bleus</label>
            <input type="number" name="blue" id="blue" min="0" max="3" required>
        <label for="yellow">Nombre de dés jaunes</label>
            <input type="number" name="yellow" id="yellow" min="0" max="3" required>
        <label for="red">Nombre de dés rouges</label>
            <input type="number" name="red" id="red" min="0" max="3" required>
        <button name="action" value="chooseDice">Choisir les dés</button>
    </form>
';
        }
    }
}


?>

