<link rel="stylesheet" href="css/accueil.css">
<main>
    <h1>Statistiques du site</h1>

    <h2>Résultat de la requête 1</h2>

    <?php global $result1;
    if ($result1 != []) { ?>
        <p>Nombre de joueurs : <?php echo $result1["nb_joueurs"] ?> </p>
        <p>Nombre d'équipes : <?php echo $result1["nb_equipes"] ?> </p>
        <p>Nombre de classements : <?php echo $result1["nb_classements"] ?> </p>
        <p>Nombre de tournois : <?php echo $result1["nb_tournois"] ?> </p>
        <p>Moyenne de participants par tournoi : <?php echo $result1["moyenne_participants_par_tournoi"] ?> </p>
    <?php } else { ?>
        <p>Erreur lors de l'exécution de la requête 1</p>
    <?php } ?>

    <h2>Résultat de la requête 2</h2>
    <?php global $result2;
    if (count($result2) > 0) {
        ?>
        <table>
            <tr>
                <th>Nom du tournoi</th>
                <th>Date de début</th>
                <th>Niveau de la phase</th>
            </tr>

            <?php foreach ($result2 as $row) { ?>
                <tr>
                    <td> <?php echo $row["nom"] ?></td>
                    <td>   <?php echo $row["dateDeb"] ?></td>
                    <td> <?php echo $row["niveaux"] ?></td>
                </tr>

            <?php } ?>
        </table>

    <?php } else { ?>
        <p>0 results</p>
    <?php } ?>

    <h2>Résultat de la requête 3</h2>
    <?php global $result3;
    if ($result3 != -1) { ?>
        <p>Le nombre de résultats obtenus est: <?php echo $result3 ?> </p>
    <?php } else { ?>
        <p>0 results</p>
    <?php } ?>

    <h2>Résultat de la requête 4</h2>
    <?php
    global $result4;
    if (count($result4) > 0) {
        ?>
        <table>
            <tr>
                <th>Année</th>
                <th>Moyenne des participants</th>
            </tr>
            <?php foreach ($result4 as $row) { ?>
                <tr>
                    <td> <?php echo $row["annee"] ?></td>
                    <td> <?php echo $row["moyenne_participants"] ?></td>
                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
        <p>0 results</p>
    <?php } ?>

    <h2>Résultat de la requête 5</h2>
    <?php
    global $result5;
    if (count($result5) > 0) {
        ?>
        <table>
            <tr>
                <th>Nom du joueur</th>
                <th>Prénom du joueur</th>
            </tr>
            <?php foreach ($result5 as $row) { ?>
                <tr>
                    <td> <?php echo $row["nom"] ?> </td>
                    <td> <?php echo $row["prenom"] ?> </td>
                </tr>

            <?php } ?>
        </table>
    <?php } else { ?>
        <p>0 results</p>
    <?php } ?>

    <h2>Résultat de la requête 6</h2>
    <?php
    global $result6;
    if (count($result6) > 0) {
        ?>
        <table>
            <tr>
                <th>Numéro de carte</th>
                <th>Nombre de parties</th>
            </tr>
            <?php
            foreach ($result6 as $row) {
                ?>
                <tr>
                    <td> <?php echo $row["nCarte"] ?> </td>
                    <td> <?php echo $row["nombre_de_parties"] ?> </td>
                </tr>

            <?php } ?>
        </table>
    <?php } else { ?>
        <p>0 results</p>
    <?php } ?>


    <h2>Résultat de la requête 7</h2>
    <?php
    global $result7;
    if (count($result7) > 0) {
        ?>
        <table>
            <tr>
                <th>Pseudo du joueur</th>
                <th>Nombre de parties</th>
            </tr>
            <?php
            foreach ($result7 as $row) { ?>
                <tr>
                    <td> <?php echo $row["pseudo"] ?> </td>
                    <td> <?php echo $row["nombre_de_parties"] ?> </td>
                </tr>
            <?php } ?>
        </table>
    <?php } else { ?>
        <p>0 results</p>
    <?php } ?>

</main>
