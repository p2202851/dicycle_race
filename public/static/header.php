<!-- Bloc entête -->
<header>

    <div class="header_logo">

        <a href="./index.php">
            <img src="img/logo.png" alt="logo"/>
        </a>

    </div>

    <div class="header_titre">

        <h1>Dicycle-Race</h1>
        <h2>Bienvenue sur ce jeu extraordinaire développé grace à ce magnifique language qu'est PHP !</h2>

    </div>

</header>