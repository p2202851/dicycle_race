<footer>
    <a href="https://creativecommons.org/licenses/" target="_blank"><img src="img/by-nc-sa-eu.png"
                                                                         alt="Licence CC BY-NC-SA"/></a>
    <span><?php print date("Y"); ?></span> - <span>Dicycle Race</span>
    <span><a href="https://perso.liris.cnrs.fr/nicolas.lumineau/ens/BDW1/" target="_blank">BDW1 - Base de données et programmation web - Partie Projet</a> - UCB Lyon 1</span>
</footer>

<footer>
    <p>Liens utiles:</p>
    <ul>
        <li><a href="https://www.univ-lyon1.fr/">Site de l'UCBL</a></li>
        <li><a href="plan-du-site.html">Plan du site</a></li>
    </ul>
    <p>Auteurs:</p>
    <ul>
        <li>LABZIZI Rayane</li>
        <li>DECHELETTE Eymeric</li>
    </ul>
    <p>Année de réalisation: 2024</p>
    <p>Copyright: Tous droits réservés.</p>
</footer>
