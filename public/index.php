<?php
ob_start();
session_start();                      // démarre ou reprend une session
require('includes/config_bd.php');
require('includes/includes.php');
require('modele/modele.php');
open_connection_DB();

$allowed_pages = ['accueil', 'afficher', 'creerPartie', "jouer", "selectionJoueurs"];
?>


<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <title>DIcycle Race</title>
</head>
<body>
<?php
include('static/header.php');
include("static/menu.php");
?>

<!-- Définition du bloc proncipal -->

<main class="main_div">
    <?php
    /* Initialisation du contrôleur et le de vue par défaut */
    $controleur = 'accueil_controleur.php';
    $vue = 'accueil_vue.php';
    $model = 'accueil_model.php';

    /* Affectation du controleur et de la vue souhaités */
    if (isset($_GET['page'])) {
        // récupération du paramètre 'page' dans l'URL
        $nomPage = $_GET['page'];
        $noLfiTryed = in_array($nomPage, $allowed_pages);
        if (!$noLfiTryed) {
            $nomPage = 'accueil';
        }
        // construction des noms de con,trôleur et de vue
        $controleur = $nomPage . '_controleur.php';
        $vue = $nomPage . '_vue.php';
        $model = $nomPage . '_model.php';
    }
    /* Inclusion du model, du contrôleur et de la vue courante */
    if (file_exists('modele/' . $model)) include('modele/' . $model);
    if (file_exists('controleurs/' . $controleur)) include('controleurs/' . $controleur);
    if (file_exists('vues/' . $vue)) {
        include('vues/' . $vue);
    } else {
        echo "La vue n'existe pas";
    }
    ?>
</main>


<?php
/* Inclusion de la partie Pied de page*/
include('static/footer.php');
?>
</body>
</html>
<?php close_connection_DB();
ob_end_flush();
?>

